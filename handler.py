import json
import base64
from argparse import Namespace
from release_generator import apply_logic


def lambda_handler(event, context):
    if 'isBase64Encoded' in event and event["isBase64Encoded"]:
        decoded = base64.b64decode(event["body"])
        body = json.loads(decoded)
    else:
        body = json.loads(event["body"])

    args = body.get('args')
    args = Namespace(**args)

    result = apply_logic(args)
    return {
        'statusCode': 200,
        'body': json.dumps({'result': result}),
        'headers': {'Content-Type': 'application/json'}
    }


# event = {
#   "args": {
#     "grupo": "random",
#     "tautograma": False,
#     "letra": None
#   }
# }

# context = None
# # args = event['args']
# # args = Namespace(**args)
# # event = {
# #     'args': args
# # }
# r = lambda_handler(event, context)
# print(r)
