import argparse
from generator import ReleaseNamesGenerator

generator = ReleaseNamesGenerator('animales.yaml', 'adjetivos.yaml')


def get_animal(argsd):
    if argsd.letra:
        argsd.tautograma = True

        return generator.get_animal_starting_with(argsd.letra[0])

    grupo = argsd.grupo

    if grupo == "random":
        animal = generator.get_random_animal_from_all()
    else:
        animal = generator.get_random_animal_from_group(grupo)

    if animal is None:
        available_groups = generator.available_animal_groups
        animal = "Sorry {} is not a valid group, try the following {}".format(grupo, available_groups)

    return animal


def get_adjective(argsb, chosen_animal):
    if argsb.tautograma:
        letra = chosen_animal[0]
        return generator.get_adjective_starting_with(letra)

    return generator.get_random_adjective_from_all()


def generate_message(chosen_animal, chosen_adjective):
    if chosen_adjective is None:
        return "Sorry we do not have adjectives starting with '{}' for '{}'\nTry again".format(chosen_animal[0], chosen_animal)

    return ' '.join((chosen_animal.capitalize(), chosen_adjective.capitalize()))


def apply_logic(argsa):
    chosen_animal = get_animal(argsa)

    if chosen_animal is None and argsa.tautograma:
        chosen_animal = "Sorry we do not have animals starting with {}".format(argsa.letra)
        chosen_adjective = ''
    else:
        chosen_adjective = get_adjective(argsa, chosen_animal)

    message = generate_message(chosen_animal, chosen_adjective)

    return message


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Release name generator')
    parser.add_argument('--letra', type=str, help='Especifica la primera letra para el animal y el adjetivo')
    parser.add_argument('--grupo', default='random', type=str, help='Especifica el tipo de animal')
    parser.add_argument('-tautograma', action='store_true')
    args = parser.parse_args()

    print(apply_logic(args))
